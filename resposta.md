Testes:
-30x30, ^10: 
	matrix_pot: 0.025848 seconds (29.91 k allocations: 1.582 MiB)
	matrix_pot_by_squaring: 0.000243 seconds (4 allocations: 28.750 KiB)
-30x30, ^20:
	matrix_pot: 0.005015 seconds (19 allocations: 136.562 KiB)
	matrix_pot_by_squaring: 0.001188 seconds (5 allocations: 35.938 KiB)
-10x10, ^50:
	matrix_pot: 0.000481 seconds (49 allocations: 42.875 KiB)
	matrix_pot_by_squaring:0.000073 seconds (7 allocations: 6.125 KiB)
-20x20, ^20:
	matrix_pot: 0.216812 seconds (594.00 k allocations: 21.552 MiB, 3.28% gc time)
	matrix_pot_by_squaring: 0.112606 seconds (99.14 k allocations: 3.535 MiB, 2.86% gc time)
-20x20, ^100:
	matrix_por: 0.004489 seconds (99 allocations: 321.750 KiB)
	matrix_pot_by_squaring:0.000257 seconds (8 allocations: 26.000 KiB)

Houve diferença nos tempos de execução? Por quê?
Sim, porque o matrix pot usa da "força bruta" e faz a função multiplica p-1 vezes, enquanto o matrix pot by squaring usa da multiplicação de potencias pelas potencias de 2 e possui complexidade log de p na base 2. 
